#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <time.h>

void rmsusfile(const char* targetdir) {
    char flog[256];
    snprintf(flog, sizeof(flog), "%s/cleaner.log", getenv("HOME"));

    while (1) {
        DIR* dir = opendir(targetdir);
        if (dir == NULL) {
            perror("Failed to open the directory");
            exit(EXIT_FAILURE);
        }

        struct dirent* entry;
        while ((entry = readdir(dir))) {
            if (entry->d_type == DT_REG) {
                char fpath[512];
                snprintf(fpath, sizeof(fpath), "%s/%s", targetdir, entry->d_name);

                FILE* file = fopen(fpath, "r");
                if (file == NULL) {
                    perror("Failed to open the file");
                    exit(EXIT_FAILURE);
                }

                char line[512];
                while (fgets(line, sizeof(line), file)) {
                    if (strstr(line, "SUSPICIOUS") != NULL) {
                        if (remove(fpath) == 0) {
                            msglog(flog, fpath);
                        } else {
                            perror("Failed to remove the file");
                            exit(EXIT_FAILURE);
                        }
                    }
                }

                fclose(file);
            }
        }

        closedir(dir);

        sleep(30);
    }
}

void msglog(const char* flog, const char* msg) {
    time_t t;
    struct tm* tmdetails;
    time(&t);
    tmdetails = localtime(&t);

    char timestamp[21];
    strftime(timestamp, 21, "%Y-%m-%d %H:%M:%S", tmdetails);

    int fd = open(flog, O_WRONLY | O_CREAT | O_APPEND, 0644);
    if (fd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    dprintf(fd, "[%s] '%s' has been removed.\n", timestamp, msg);
    close(fd);
}

int main(int argc, char* argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <directory_path>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    const char* directory_path = argv[1];
    if (access(directory_path, F_OK) == -1) {
        perror("access");
        exit(EXIT_FAILURE);
    }

    pid_t pid = fork();
    if (pid < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    if (setsid() < 0) {
        perror("setsid");
        exit(EXIT_FAILURE);
    }

    if (chdir("/") < 0) {
        perror("chdir");
        exit(EXIT_FAILURE);
    }

    umask(0);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    rmsusfile(directory_path);

    return 0;
}
