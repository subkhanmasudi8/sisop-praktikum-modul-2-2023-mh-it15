#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include <dirent.h>
#include <sys/wait.h>

int main() {
	const char *foldername = "players";
	const char *zipfilename = "players.zip";
	const char *downloadurl = "https://drive.google.com/uc?export=download&id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK";
	const char *teamname = "Cavaliers";
	const char *positions[] = {"PG", "SG", "SF", "PF", "C"};
	int playercounts[5] = {0};

	mkdir(foldername, 0777);

	pid_t pid = fork();

	if (pid == 0) { 
		execlp("wget", "wget", "--no-check-certificate", "-O", zipfilename, downloadurl, NULL);
		} else { 
        	int childstatus;
        	wait(&childstatus);
    	}

    	pid_t unzipPid = fork();

    	if (unzipPid == 0) {
        	execlp("unzip", "unzip", zipfilename, "-d", foldername, NULL);
    		} else { 
        		int childstatus;
        		wait(&childstatus);
    		}

    	remove(zipfilename);

    	DIR *dir = opendir(foldername);

    	struct dirent *entry;
    	while ((entry = readdir(dir)) != NULL) {
        	if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            	continue;
        	}

        if (strstr(entry->d_name, teamname) == NULL) {
            	char filepath[512];
            	snprintf(filepath, sizeof(filepath), "%s/%s", foldername, entry->d_name);
            	remove(filepath);
        	}
		}

    	rewinddir(dir); 

    	while ((entry = readdir(dir)) != NULL) {
        	if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            	continue;
        	}

        	char filepath[512];
        	snprintf(filepath, sizeof(filepath), "%s/%s", foldername, entry->d_name);

        	int position = -1;
        	for (int i = 0; i < 5; i++) {
            	if (strstr(entry->d_name, positions[i]) != NULL) {
                	position = i;
                	break;
            	}
        	}

        	if (position != -1) {
            	const char *positionfolder = positions[position];
            	char destinationfolder[512];
            	snprintf(destinationfolder, sizeof(destinationfolder), "%s/%s", foldername, positionfolder);

            	mkdir(destinationfolder, 0777);

            	char destinationpath[512];
            	int result = snprintf(destinationpath, sizeof(destinationpath), "%s/%s", destinationfolder, entry->d_name);

            	if (result < 0 || result >= sizeof(destinationpath)) {
                	return 1;
            	}

            	rename(filepath, destinationpath);
            	playercounts[position]++;
        	}
    	}

	closedir(dir);

    	const char *clutchfolder = "clutch";
    	mkdir(clutchfolder, 0777);

    	const char *sourceKyrie = "players/PG/Cavaliers-PG-Kyrie-Irving.png";
    	const char *destinationKyrie = "clutch/Cavaliers-PG-Kyrie-Irving.png";
    	rename(sourceKyrie, destinationKyrie);

    	const char *sourceLeBron = "players/SF/Cavaliers-SF-LeBron-James.png";
    	const char *destinationLeBron = "clutch/Cavaliers-SF-LeBron-James.png";
    	rename(sourceLeBron, destinationLeBron);

    	FILE *formasifile = fopen("Formasi.txt", "w");

    	for (int i = 0; i < 5; i++) {
        	fprintf(formasifile, "%s: %d\n", positions[i], playercounts[i]);
    	}

    	fclose(formasifile);

    	return 0;
}
