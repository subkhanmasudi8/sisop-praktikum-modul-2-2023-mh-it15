#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>

void getTimestamp(char *timestamp) {
    time_t rawtime;
    struct tm *timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(timestamp, 20, "%d-%m-%y:%H-%M-%S", timeinfo);
}

void decryptExtensions(char *filename, FILE *logFile, int protectionLevel) {
    FILE *inputFile = fopen(filename, "r");

    if (!inputFile) {
        printf("Gagal membuka file.\n");
        return;
    }

    char line[100];
    int lineCount = 0;

    char timestamp[20];
    getTimestamp(timestamp);

    while (fgets(line, sizeof(line), inputFile)) {
        if (lineCount < 8) {
            printf("%s", line);
            fprintf(logFile, "[%s] - %s - %s - Detected\n", "sisopUser", timestamp, line);

            if (protectionLevel == 2) {
                fprintf(logFile, "[%s] - %s - %s - Moved to quarantine\n", "sisopUser", timestamp, line);
                
            } else if (protectionLevel == 3) {
                fprintf(logFile, "[%s] - %s - %s - Deleted\n", "sisopUser", timestamp, line);
            }
        } else {
            for (int i = 0; i < strlen(line); i++) {
                if ((line[i] >= 'a' && line[i] <= 'z') || (line[i] >= 'A' && line[i] <= 'Z')) {
                    line[i] = (line[i] >= 'a' && line[i] <= 'z') ? (((line[i] - 'a' + 13) % 26) + 'a') : (((line[i] - 'A' + 13) % 26) + 'A');
                }
            }
            printf("%s", line);

            fprintf(logFile, "[%s] - %s - %s - Detected\n", "sisopUser", timestamp, line);

            if (protectionLevel == 2) {
                fprintf(logFile, "[%s] - %s - %s - Moved to quarantine\n", "sisopUser", timestamp, line);
               
            } else if (protectionLevel == 3) {
                fprintf(logFile, "[%s] - %s - %s - Deleted\n", "sisopUser", timestamp, line);
               
            }
        }

        lineCount++;
    }

    fclose(inputFile);
}

void *backgroundScanner(void *arg) {
    int protectionLevel = *((int *)arg);

    FILE *logFile = fopen("virus.log", "a");

    if (!logFile) {
        printf("Gagal membuka virus.log.\n");
        exit(EXIT_FAILURE);
    }

    while (1) {
        decryptExtensions("sisop_infected/extensions.csv", logFile, protectionLevel);
        sleep(1); 

    }

    fclose(logFile);
    return NULL;
}

int main(int argc, char *argv[]) {
    if (argc != 3 || (strcmp(argv[1], "-p") != 0)) {
        printf("Penggunaan: %s -p <low|medium|hard>\n", argv[0]);
        return 1;
    }

    int protectionLevel;
    if (strcmp(argv[2], "low") == 0) {
        protectionLevel = 1;
    } else if (strcmp(argv[2], "medium") == 0) {
        protectionLevel = 2;
    } else if (strcmp(argv[2], "hard") == 0) {
        protectionLevel = 3;
    } else {
        printf("Penggunaan: %s -p <low|medium|hard>\n", argv[0]);
        return 1;
    }

    pthread_t thread;
    pthread_create(&thread, NULL, backgroundScanner, (void *)&protectionLevel);

    printf("Tekan Ctrl+C untuk menghentikan program.\n");
    pthread_join(thread, NULL);

    return 0;
}
