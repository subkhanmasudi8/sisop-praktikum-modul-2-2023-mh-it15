#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <signal.h>
#include <errno.h>
#include <wait.h>

#define MODE_A 0
#define MODE_B 1

int mode = MODE_A;

void create_directory(const char *dir_name) {
    mkdir(dir_name);
}

void download_image(const char *dir_name) {
    time_t current_time;
    struct tm *time_info;
    char timestamp[20];

    time(&current_time);
    time_info = localtime(&current_time);
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", time_info);

    char url[256];
    sprintf(url, "https://source.unsplash.com/%dx%d", (int)(current_time % 1000) + 50, (int)(current_time % 1000) + 50);

    char filename[256];
    snprintf(filename, sizeof(filename), "%s/%s.jpg", dir_name, timestamp);

    pid_t pid = fork();

    if (pid == 0) {
        int fd = open(filename, O_WRONLY | O_CREAT, 0644);
        if (fd < 0) {
            perror("open");
            exit(1);
        }

        dup2(fd, STDOUT_FILENO);
        close(fd);

        execlp("wget", "wget", "-q", "-O", "-", url, NULL);
        exit(1);
    }
}

void zip_directory(const char *dir_name) {
    pid_t pid = fork();

    if (pid == 0) {
        chdir(dir_name);

        char zip_filename[256];
        snprintf(zip_filename, sizeof(zip_filename), "%s.zip", dir_name);

        execlp("zip", "zip", "-r", "-q", zip_filename, ".", NULL);
        exit(1);
    }

    waitpid(pid, NULL, 0);
}

void delete_directory(const char *dir_name) {
    pid_t pid = fork();

    if (pid == 0) {
        execlp("rm", "rm", "-r", dir_name, NULL);
        exit(1);
    }

    waitpid(pid, NULL, 0);
}

void create_killer_program() {
    char killer_code[] = "#include <stdio.h>\n#include <stdlib.h>\n#include <signal.h>\n\n"
                        "int main() {\n"
                        "    kill(0, SIGTERM);\n"
                        "    remove(\"killer\");\n"
                        "    return 0;\n"
                        "}\n";

    FILE *killer_file = fopen("killer.c", "w");
    fprintf(killer_file, "%s", killer_code);
    fclose(killer_file);

    pid_t pid = fork();

    if (pid == 0) {
        execlp("gcc", "gcc", "-o", "killer", "killer.c", NULL);
        exit(1);
    }

    waitpid(pid, NULL, 0);
}

void cleanup() {
    if (mode == MODE_B) {
        int status;
        while (wait(&status) > 0);
    }

    delete_directory("killer");
}

void handle_sigterm(int signum) {
    cleanup();
    exit(0);
}

int main(int argc, char *argv[]) {
    if (argc != 2 || (strcmp(argv[1], "-a") != 0 && strcmp(argv[1], "-b") != 0)) {
        fprintf(stderr, "Usage: %s -a (for MODE_A) or %s -b (for MODE_B)\n", argv[0], argv[0]);
        return 1;
    }

    if (strcmp(argv[1], "-b") == 0) {
        mode = MODE_B;
    }

    signal(SIGTERM, handle_sigterm);

    create_killer_program();

    while (1) {
        char dir_name[256];
        time_t current_time;
        struct tm *time_info;
        char timestamp[20];

        time(&current_time);
        time_info = localtime(&current_time);
        strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", time_info);

        create_directory(timestamp);

        int image_count = 0;

        while (image_count < 15) {
            download_image(timestamp);
            sleep(5);
            image_count++;
        }

        zip_directory(timestamp);
        delete_directory(timestamp);
        sleep(30);
    }

    return 0;
}

