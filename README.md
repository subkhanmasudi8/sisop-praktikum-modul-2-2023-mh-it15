# sisop-praktikum-modul-2-2023-MH-IT15
Laporan Resmi pengerjaan soal shift modul 1 Praktikum Sistem Operasi 2023 Kelompok IT15

## Anggota Kelompok
1. Ilhan Ahmad Syafa (5027221040)
2. Subkhan Masudi (5027221044)
3. Gilang Raya Kurniawan (5027221045)

# SOAL 1

John adalah seorang mahasiswa biasa. Pada tahun ke-dua kuliahnya, dia merasa bahwa dia telah menyianyiakan waktu kuliahnya selama ini. Selama mengerjakan tugas, John selalu menggunakan bantuan ChatGPT dan tidak pernah mempelajari apapun dari hal tersebut. Untuk itu, pada soal kali ini John bertekad untuk tidak menggunakan ChatGPT dan mencoba menyelesaikan tugasnya dengan tangan dan pikirannya sendiri. Melihat tekad yang kuat dari John, Mark, dosen yang mengajar John, ingin membantunya belajar dengan memberikan sebuah ujian. Sebelum memberikan ujian pada John, Mark berpesan bahwa John harus bersungguh-sungguh dalam mengerjakan ujian, fokus untuk belajar, dan tidak perlu khawatir akan nilai yang diberikan. Mark memberikan ujian pada John untuk membuatkannya sebuah program `cleaner` sederhana dengan ketentuan berikut.

## Problem Soal 1

A. Program tersebut menerima input path dengan menggunakan “argv”.

B. Program tersebut bertugas untuk menghapus file yang didalamnya terdapat string "SUSPICIOUS" pada direktori yang telah diinputkan oleh user.

C. Program tersebut harus berjalan secara daemon.

D. Program tersebut akan terus berjalan di background dengan jeda 30 detik.

E. Dalam pembuatan program tersebut, tidak diperbolehkan menggunakan fungsi `system()`.

F. Setiap kali program tersebut menghapus sebuah file, maka akan dicatat pada file 'cleaner.log' yang ada pada direktori home user dengan format seperti berikut : “[YYYY-mm-dd HH:MM:SS] '<absolute_path_to_file>' has been removed.”.

## Solution Soal 1

[Source Code Soal 1](https://gitlab.com/subkhanmasudi8/sisop-praktikum-modul-2-2023-mh-it15/-/tree/main/soal_1?ref_type=heads)

### Solution Soal 1 A
```
int main(int argc, char* argv[])
```
Dalam deklarasi fungsi `int main(int argc, char* argv[])`parameter argv adalah array dari pointer ke karakter (char*) yang digunakan untuk menyimpan argumen baris perintah yang diberikan kepada program saat program dijalankan. Jadi nanti user dapat menginputkan alamat path yang ingin ia cek melalui program `cleaner` ini.

### Solution Soal 1 B
```
char line[512];
while (fgets(line, sizeof(line), file)) {
    if (strstr(line, "SUSPICIOUS") != NULL) {
        if (remove(fpath) == 0) {
            msglog(flog, fpath);
        } else {
            perror("Failed to remove the file");
            exit(EXIT_FAILURE);
        }
```
Program akan melakukan loop untuk mengecek tiap baris pada file yang dibuat di direktori yang ditentukan. Di dalam loop, program menggunakan fungsi `strstr` untuk mencari apakah kata **SUSPICIOUS** ada di dalam baris yang sedang dibaca (line). Jika kata ini ditemukan, maka kondisi ini akan terpenuhi dan kode di dalam blok if akan dieksekusi. Kemudian program akan menghapus file menggunakan fungsi `remove`. Jika file berhasil dihapus, program akan memanggil fungsi `msglog` dengan parameter flog dan fpath. Fungsi ini mungkin bertugas untuk mencatat pesan log yang mencatat bahwa file telah dihapus.

### Solution Soal 1 C
```
pid_t pid = fork();
    if (pid < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    if (setsid() < 0) {
        perror("setsid");
        exit(EXIT_FAILURE);
    }

    if (chdir("/") < 0) {
        perror("chdir");
        exit(EXIT_FAILURE);
    }

    umask(0);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    rmsusfile(directory_path);
```
Program akan menjalankan proses child yang menjalankan fungsi `rmsusfile(targetdir)`. Jika proses parent (pid > 0), maka proses ini akan keluar dengan status keluar **EXIT_SUCCES**. Ini dilakukan untuk memastikan bahwa hanya proses child yang menjalankan kode selanjutnya. Program menutup file descriptor standar (stdin, stdout, dan stderr) yang terkait dengan proses parent. Ini dilakukan untuk memastikan bahwa program tidak memiliki koneksi I/O dengan terminal. Dengan menutup file descriptor ini, program menjadi lebih independen dan tidak terkait dengan terminal sehingga dapat berjalan secara daemon.

### Solution Soal 1 D
```
while (1) {
    // ...
    sleep(30);
}
```
Di dalam loop tersebut, setelah selesai memproses direktori, program menggunakan `sleep(30)` untuk membuat program tidur selama 30 detik sebelum melanjutkan kembali.

### Solution Soal 1 E 
Sesuai dengan source code soal 1 di atas, program `cleaner` ini tidak menggunakan fungsi `system()` di dalam prosesnya.

### Solution Soal 1 F 
```
void msglog(const char* flog, const char* msg) {
    time_t t;
    struct tm* tmdetails;
    time(&t);
    tmdetails = localtime(&t);

    char timestamp[21];
    strftime(timestamp, 21, "%Y-%m-%d %H:%M:%S", tmdetails);

    int fd = open(flog, O_WRONLY | O_CREAT | O_APPEND, 0644);
    if (fd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    dprintf(fd, "[%s] '%s' has been removed.\n", timestamp, msg);
    close(fd);
}

void rmsusfile(const char* targetdir) {
    char flog[256];
    snprintf(flog, sizeof(flog), "%s/cleaner.log", getenv("HOME"));
```
Kedua fungsi tersebut akan saling terkait dalam proses pembuatan file `cleaner.log` setelah proses penghapusan file berjalan. Fungsi `msglog` berfungsi untuk menulis isi dari file `cleaner.log` sesuai dengan ketentuan pada soal. Fungsi `rmsusfile` berfungsi untuk mengecek file secara looping apakah terdapat kata **SUSPICIOUS** atau tidak. Jika iya, maka log pesan akan dicatat pada file `cleaner.log`.

## Pengerjaan Soal 1
<a href="https://ibb.co/V9K4z71"><img src="https://i.ibb.co/GphS8K6/step1.png" alt="step1" border="0"></a> <br>

Pertama, buat sebuah direktori. Kemudian buatlah file `cleaner.c` yang berisi source code di atas. Lalu compile file tersebut agar bisa dieksekusi dengan perintah `gcc cleaner.c -o cleaner`. Jalankan program pada direktori yang diinginkan. Disini kami membuat sebuah file bernama **hapusaja.txt** dan **janganhapus.txt** yang masing-masing isinya seperti gambar berikut.

<a href="https://ibb.co/KW2QrbF"><img src="https://i.ibb.co/Pgcb6Fw/isifilehapus.png" alt="isifilehapus" border="0"></a>

<a href="https://ibb.co/wC6hTjL"><img src="https://i.ibb.co/ZKTz37J/isifilejgnhapus.png" alt="isifilejgnhapus" border="0"></a> <br>

Kemudian, cek direktori target dari program yang telah berjalan secara daemon tersebut dengan menggunakan perintah `ls` untuk melihat apakah file berhasil terhapus.

<a href="https://ibb.co/jVmdMWW"><img src="https://i.ibb.co/M8rbVGG/step2.png" alt="step2" border="0"></a><br />

Dari gambar terlihat bahwa file **hapusaja.txt** telah berhasil terhapus karena mengandung kata **SUSPICIOUS**. Hal ini dibuktikan pada isi file `cleaner.log` yang menampilkan timestamp dan direktori dari file yang terhapus.

# SOAL 2

QQ adalah fan Cleveland Cavaliers. Ia ingin memajang kamarnya dengan poster foto roster Cleveland Cavaliers tahun 2016. Maka yang dia lakukan adalah meminta tolong temannya yang sangat sisopholic untuk membuatkannya sebuah program untuk mendownload gambar - gambar pemain tersebut. Sebagai teman baiknya, bantu QQ untuk mencarikan foto foto yang dibutuhkan QQ dengan ketentuan sebagai berikut:

## Problem Soal 2

- Pertama, buatlah program bernama “cavs.c” yang dimana program tersebut akan membuat folder “players”.

- Program akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip” ke dalam folder players yang telah dibuat. Lalu hapus file zip tersebut agar tidak memenuhi komputer QQ.

- Dikarenakan database yang diunduh masih data mentah. Maka bantulah QQ untuk menghapus semua pemain yang bukan dari Cleveland Cavaliers yang ada di directory.

- Setelah mengetahui nama-nama pemain Cleveland Cavaliers, QQ perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka   dalam waktu bersamaan dengan 5 proses yang berbeda. Untuk kategori folder akan menjadi 5 yaitu point guard (PG), shooting guard (SG), small forward (SF), power forward (PF), dan center (C).

- Hasil kategorisasi akan di outputkan ke file Formasi.txt, dengan berisi
    1. PG: {jumlah pemain}
    2. SG: {jumlah pemain}
    3. SF: {jumlah pemain}
    4. PF: {jumlah pemain}
    5. C: {jumlah pemain}

- Ia ingin memajang foto pemain yang menembakkan game-winning shot pada ajang NBA Finals 2016, tepatnya pada game 7, dengan membuat folder “clutch”, yang di dalamnya berisi foto pemain yang bersangkutan.

- Ia merasa kurang lengkap jika tidak memajang foto pemain yang melakukan The Block pada ajang yang sama, Maka dari itu ditaruhlah foto pemain tersebut di folder “clutch” yang sama.
	
    **Catatan**:

- Format nama file yang akan diunduh dalam zip berupa [tim]-[posisi]-[nama].png

- Tidak boleh menggunakan system(), Gunakan exec() dan fork().

- Directory “.” dan “..” tidak termasuk yang akan dihapus.
- 2 poin soal terakhir dilakukan setelah proses kategorisasi selesai

## Solution Soal 2

[Source Code Soal 2](https://gitlab.com/subkhanmasudi8/sisop-praktikum-modul-2-2023-mh-it15/-/tree/main/soal_2?ref_type=heads)

### Solution Soal 2 A

Pertama tama buat dahulu program bernama **“cavs.c"**.
```
int main() {
	const char *foldername = "players";
	const char *zipfilename = "players.zip";
	const char *downloadurl = "https://drive.google.com/uc?export=download&id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK";
	const char *teamname = "Cavaliers";
	const char *positions[] = {"PG", "SG", "SF", "PF", "C"};
	int playercounts[5] = {0};

	mkdir(foldername, 0777);
```
pada code ```int main()``` berfungsi mendeklarasi dari fungsi utama program yang akan dibuat, sedangkan kode kode nya sebgai berikut.
Code Code dibawah ini berfungsi sebagai berikut :

    const char *foldername: Variabel yang membaut folder dan namanya
    const char *zipfilename: Variabel yang membuat nama  ZIP yang akan diunduh.
    const char *downloadurl: Variabel URL yang akan digunakan untuk mengunduh ZIP.
    const char *teamname: Variabel nama tim yang tidak dihapus.
    const char *positions[]: Variabel daftar posisi pemain.
    int playercounts[5]: Array untuk menghitung jumlah pemain dalam setiap posisi.

```mkdir(foldername, 0777)``` berfungsi untuk memberikan akses penuh kepada folder yang akan dibuat pada code```const char *foldername = "players";)```. pada code diatas program akan membaut folder player, **folder player akan disimpan ditempat yang sama dimana program “cavs.c" dibuat**

### Solution Soal 2 B

Program akan melakukan pengunduhan dengan link yang sudah di deklarasikan pada 	```const char *downloadurl = "https://drive.google.com/uc?export=download id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK";``` dengaan menggunakan wget program akan melakukan pengunduhan, code ```--no-check-certificate```digunakan untuk mengabaikan pemeriksaan sertifikat SSL saat mengunduh dari URL, sedangkan code ```"-O", zipfilename```  digunakan untuk menentukan nama berkas hasil unduhan yaitu bernama **players.zip**.
```
	if (pid == 0) { 
		execlp("wget", "wget", "--no-check-certificate", "-O", zipfilename, downloadurl, NULL);
		} else { 
        	int childstatus;
        	wait(&childstatus);
    	}
```
Selanjutnya program akan melakukan ekstraksi/unzip file **players.zip**. dengan perintah dibawah.
```
    	if (unzipPid == 0) {
        	execlp("unzip", "unzip", zipfilename, "-d", foldername, NULL);
    		} else { 
        		int childstatus;
        		wait(&childstatus);
    		}
```
Selanjutnya kode dibawah digunakan untuk menghapus **players.zip** keitika kode sudah dijalankan.
```
    	remove(zipfilename);
```
### Solution Soal 2 C

```
struct dirent *entry;
    	while ((entry = readdir(dir)) != NULL) {
        	if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            	continue;
        	}

        if (strstr(entry->d_name, teamname) == NULL) {
            	char filepath[512];
            	snprintf(filepath, sizeof(filepath), "%s/%s", foldername, entry->d_name);
            	remove(filepath);
        	}
		}
```
Program diatas digunakan untuk menggunakan menghapus semua pemain yang bukan dari tim **Cleveland Cavaliers** yang ada di directory player, diimana code const ```char *teamname = "Cavaliers";``` mendeklarasikan pemain dengan code **Cavaliers** jadi pemain yang tidak ada kata **Cavaliers** dalam filenya akan di hapus oleh program

### Solution Soal 2 D

```
while ((entry = readdir(dir)) != NULL) {
        	if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            	continue;
        	}

        	char filepath[512];
        	snprintf(filepath, sizeof(filepath), "%s/%s", foldername, entry->d_name);

        	int position = -1;
        	for (int i = 0; i < 5; i++) {
            	if (strstr(entry->d_name, positions[i]) != NULL) {
                	position = i;
                	break;
            	}
        	}

        	if (position != -1) {
            	const char *positionfolder = positions[position];
            	char destinationfolder[512];
            	snprintf(destinationfolder, sizeof(destinationfolder), "%s/%s", foldername, positionfolder);

            	mkdir(destinationfolder, 0777);

            	char destinationpath[512];
            	int result = snprintf(destinationpath, sizeof(destinationpath), "%s/%s", destinationfolder, entry->d_name);

            	if (result < 0 || result >= sizeof(destinationpath)) {
                	return 1;
            	}

            	rename(filepath, destinationpath);
            	playercounts[position]++;
        	}
```
Pada kode diatas program tersebut membaca setiap file dalam direktori sumber yaitu **player**. Pertama, program menggunakan loop **while** untuk **melakukan loop pada directory**. Loop ini akan terus berjalan selama masih ada file yang harus diproses dalam direktori tersebut. Kemudian, program membuat path menggunakan variabel **filepath**. Ini dilakukan dengan menggabungkan foldername yaitu **player** dengan ```entry->d_name``` yaitu **point guard (PG), shooting guard (SG), small forward (SF), power forward (PF), dan center (C).** Selanjutnya, program mencoba mencocokkan nama file dengan salah satu dari lima posisi yang disimpan dalam array ```positions```. misal **file Cavaliers-PF-Kevin-Love.png akan disimpan pada folder PF**. setalah file berhasil di kategorikan selanjutnyaa program akan menyimpan file ke folder tujuan menggunakan variabel ```destinationfolder```. Jika belum ada, program akan membuatnya. Selanjutnya, program akan menghitung jumlah pemain pada posisi yang sesuai dan menyimpan informasi ini dalam array **playercounts**.

### Solution Soal 2 E

```
    	FILE *formasifile = fopen("Formasi.txt", "w");

    	for (int i = 0; i < 5; i++) {
        	fprintf(formasifile, "%s: %d\n", positions[i], playercounts[i]);
    	}
```
Pada kode ini program akan menghitung jumlah player dan mengkategorikan seperti dibawah
- PG: {jumlah pemain}
- SG: {jumlah pemain}
- SF: {jumlah pemain}
- PF: {jumlah pemain}
- C: {jumlah pemain}

Pertama, kode membuka file ```Formasi.txt``` dalam fooder **player**,jika file tersebut sudah ada, isinya akan diganti, dan jika tidak ada, file baru akan dibuat. Kemudian, program melakukan pengulangan lima kali menggunakan loop **for**, untuk menulis lima baris data ke dalam file tadi. Setiap baris **mencakup nama posisi pemain misal = S atau PG** dan jumlah pemain dalam posisi tersebut ditulis menggunakan fungsi **fprintf dengan format posisi pemain misal C: 5** . Jadi, hasil dari kode ini tergantung pada nilai dalam array **positions** dan **playercounts** pada setiap perulangan.

### Solution Soal 2 F dan G
```
    	const char *clutchfolder = "clutch";
    	mkdir(clutchfolder, 0777);

    	const char *sourceKyrie = "players/PG/Cavaliers-PG-Kyrie-Irving.png";
    	const char *destinationKyrie = "clutch/Cavaliers-PG-Kyrie-Irving.png";
    	rename(sourceKyrie, destinationKyrie);

    	const char *sourceLeBron = "players/SF/Cavaliers-SF-LeBron-James.png";
    	const char *destinationLeBron = "clutch/Cavaliers-SF-LeBron-James.png";
    	rename(sourceLeBron, destinationLeBron);
```
Selanjutnya pada kode ini program akan membuat folder cluth dengan variabel **0777** yang berfungsi untuk memberikan akses penuh kepada folder yang akan dibuat. Selanjutnya program akan memindah **file Cavaliers-PG-Kyrie-Irving.png dan Cavaliers-SF-LeBron-James.png** dari folder masing masing Kemudian dipindahkan ke folder clutch, sesuai dengan soal.
 
## Pengerjaan Soal 2

### Solution Soal 2 A

Pertama-tama kita buat file ```cavs.c``` terlebih dahulu. lalu compile file ```cavs.c``` dengan menggunakan command ```gcc casv.c -o casv```, lalu excecute comiler tadi dengan ```./casv```, selanjutnya

<a href="https://ibb.co/NZzZJjW"><img src="https://i.ibb.co/nzSzYcQ/1.png" alt="1" border="0"></a>

program akan membuat folder **player** dahulu

### Pengerjaan Soal 2 B

<a href="https://ibb.co/qJgxtxD"><img src="https://i.ibb.co/HhPXWXK/2.png" alt="2" border="0"></a>

Lalu program akan mendownload file **Player.zip** dari link https://drive.google.com/file/d/1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK/view berikut.

<a href="https://ibb.co/NZzZJjW"><img src="https://i.ibb.co/nzSzYcQ/1.png" alt="1" border="0"></a>

Lalu program akan menghapus file zip **Player.zip** tadi.

### Pengerjaan Soal 2 C

<a href="https://ibb.co/FJdTnnK"><img src="https://i.ibb.co/S0ZCJJP/3.png" alt="3" border="0"></a>

Selanjutnya program akan menghapus file .png yang tidak mengandung nama **Cavaliers** dalam namanya.

<a href="https://ibb.co/r5NB4Tf"><img src="https://i.ibb.co/XFMnSNk/4.png" alt="4" border="0"></a>

### Pengerjaan Soal 2 D

Selanjutnya programa akan membaut **mengkategorikan pemain tersebut sesuai dengan posisi merka yang sebenarnya**. Untuk kategori folder akan **menjadi 5**, yaitu **point guard (PG), shooting guard (SG), small forward (SF), power forward (PF), dan center (C).**

<a href="https://ibb.co/rv9xdJ1"><img src="https://i.ibb.co/N7R9TzH/5.png" alt="5" border="0"></a>

Contohnya folder dan file didalamnya

<a href="https://ibb.co/vjHzqV8"><img src="https://i.ibb.co/hmy89Lv/6.png" alt="6" border="0"></a>

### Pengerjaan Soal 2 E

Lalu hasil kategorisasi yang dibuat program akan di simpan dalam **format Formasi.txt**

<a href="https://imgbb.com/"><img src="https://i.ibb.co/88hfkt3/7.png" alt="7" border="0"></a>

Contoh hasil dari kategorisasi yang disimpan dalam** Formasi.txt.**

<a href="https://ibb.co/prm0VkR"><img src="https://i.ibb.co/3YJr6PM/8.png" alt="8" border="0"></a>

### Pengerjaan Soal 2 F dan G

Lalu selanjutnya karena sang pembuat program **ingin memajang foto pemain yang menembakkan game-winning shot pada ajang NBA Finals 2016, tepatnya pada game 7 dan pemain yang melakukan The Block pada ajang yang sama**. Maka pertama tama program akan membuat folder clutch terlebih dahulu

<a href="https://ibb.co/99yXQf0"><img src="https://i.ibb.co/GvCwz1D/9.png" alt="9" border="0"></a>

Lalu memindahkan pemain yang bersangkutan ke folder clutch. 

<a href="https://ibb.co/z5G6N8K"><img src="https://i.ibb.co/yRW4VhL/10.png" alt="10" border="0"></a>




# SOAL 3

Albedo adalah seorang seniman terkenal dari Mondstadt. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini Albedo sedang menghadapi creativity block. Sebagai teman berkebangsaan dari Fontaine yang jago sisop, bantu Albedo untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

## Problem Soal 3

A. Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

B. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://source.unsplash.com/{widthxheight} , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

C. Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip, format nama [YYYY-mm-dd_HH:mm:ss].zip tanpa “[]”).

D. Karena takut program tersebut lepas kendali, Albedo ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

E. Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

    Catatan :
    1. Tidak boleh menggunakan system()
    2. Proses berjalan secara daemon
    3. Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

## Solution Soal 3

[Source Code Soal 3](https://gitlab.com/subkhanmasudi8/sisop-praktikum-modul-2-2023-mh-it15/-/tree/main/soal_3?ref_type=heads)

### Solution Soal 3 A
```
void create_directory(const char *dir_name) {
    mkdir(dir_name);
}

// // //

while (1) {
        char dir_name[256];
        time_t current_time;
        struct tm *time_info;
        char timestamp[20];

        time(&current_time);
        time_info = localtime(&current_time);
        strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", time_info);

        create_directory(timestamp);

        int image_count = 0;

        while (image_count < 15) {
            download_image(timestamp);
            sleep(5);
            image_count++;
        }

        zip_directory(timestamp);
        delete_directory(timestamp);
        sleep(30);
    }
}

```
Pada program C tersebut, pertama kami membuat fungsi membuat direktori baru yaitu fungsi `create_directory()`. Kemudian tiap 30 detik, program akan membuat folder baru. Hal itu dibuktikan dengan adanya fungsi perulangan `while()` dan di akhir terdapat fungsi `sleep(30)`. Program tersebut juga membuat folder dengan format nama seperti pada soal.

### Solution Soal 3 B
```
void download_image(const char *dir_name) {
    time_t current_time;
    struct tm *time_info;
    char timestamp[20];

    time(&current_time);
    time_info = localtime(&current_time);
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", time_info);

    char url[256];
    sprintf(url, "https://source.unsplash.com/%dx%d", (int)(current_time % 1000) + 50, (int)(current_time % 1000) + 50);

    char filename[256];
    snprintf(filename, sizeof(filename), "%s/%s.jpg", dir_name, timestamp);

    pid_t pid = fork();

    if (pid == 0) {
        int fd = open(filename, O_WRONLY | O_CREAT, 0644);
        if (fd < 0) {
            perror("open");
            exit(1);
        }

        dup2(fd, STDOUT_FILENO);
        close(fd);

        execlp("wget", "wget", "-q", "-O", "-", url, NULL);
        exit(1);
    }
}

//...

while (image_count < 15) {
            download_image(timestamp);
            sleep(5);
            image_count++;
        }
```
Program akan mendownload foto dari url yang tersedia dengan ukuran yang sesuai dengan ketentuan pada soal. Kemudian program tersebut juga akan memberi nama file foto yang telah terdownload dengan format timestamp sesuai ketentuan pada soal. Terdapat pula fungsi perulangan untuk mendownload foto tiap 5 detik selama jumlah fotonya masih kurang dari 15 foto per folder.


### Solution Soal 3 C
```
void zip_directory(const char *dir_name) {
    pid_t pid = fork();

    if (pid == 0) {
        chdir(dir_name);

        char zip_filename[256];
        snprintf(zip_filename, sizeof(zip_filename), "%s.zip", dir_name);

        execlp("zip", "zip", "-r", "-q", zip_filename, ".", NULL);
        exit(1);
    }

    waitpid(pid, NULL, 0);
}

void delete_directory(const char *dir_name) {
    pid_t pid = fork();

    if (pid == 0) {
        execlp("rm", "rm", "-r", dir_name, NULL);
        exit(1);
    }

    waitpid(pid, NULL, 0);
}
```
Setelah terisi oleh 15 foto, fungsi `zip_directory` akan menzip tiap folder yang berisi 15 foto tersebut dan menamainya dengan format nama **timestamp.zip** seperti nama folder. Kemudian, setelah folder berhasil dizip, fungsi `delete_directory` akan melakukan tugasnya untuk menghapus folder yang sudah tidak terpakai sehingga hanya tersisa file `.zip`.

### Solution Soal 3 D & E
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <signal.h>
#include <errno.h>
#include <wait.h>

#define MODE_A 0
#define MODE_B 1

int mode = MODE_A;

//...

void create_killer_program() {
    char killer_code[] = "#include <stdio.h>\n#include <stdlib.h>\n#include <signal.h>\n\n"
                        "int main() {\n"
                        "    kill(0, SIGTERM);\n"
                        "    remove(\"killer\");\n"
                        "    return 0;\n"
                        "}\n";

    FILE *killer_file = fopen("killer.c", "w");
    fprintf(killer_file, "%s", killer_code);
    fclose(killer_file);

    pid_t pid = fork();

    if (pid == 0) {
        execlp("gcc", "gcc", "-o", "killer", "killer.c", NULL);
        exit(1);
    }

    waitpid(pid, NULL, 0);
}

void cleanup() {
    if (mode == MODE_B) {
        int status;
        while (wait(&status) > 0);
    }

    delete_directory("killer");
}

void handle_sigterm(int signum) {
    cleanup();
    exit(0);
}

int main(int argc, char *argv[]) {
    if (argc != 2 || (strcmp(argv[1], "-a") != 0 && strcmp(argv[1], "-b") != 0)) {
        fprintf(stderr, "Usage: %s -a (for MODE_A) or %s -b (for MODE_B)\n", argv[0], argv[0]);
        return 1;
    }

    if (strcmp(argv[1], "-b") == 0) {
        mode = MODE_B;
    }

    signal(SIGTERM, handle_sigterm);

    create_killer_program();

    while (1) {
        char dir_name[256];
        time_t current_time;
        struct tm *time_info;
        char timestamp[20];

        time(&current_time);
        time_info = localtime(&current_time);
        strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", time_info);

        create_directory(timestamp);

        int image_count = 0;

        while (image_count < 15) {
            download_image(timestamp);
            sleep(5);
            image_count++;
        }

        zip_directory(timestamp);
        delete_directory(timestamp);
        sleep(30);
    }

    return 0;
}
```
Program akan dapat dijalankan dalam dua mode, yaitu **Mode A** yang dijalankan dengan argumen **-a** dan **Mode B** yang dijalankan dengan argumen **-b**. Ketika dijalankan dalam **ModeA**, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk **Mode B**, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai (semua folder terisi gambar, terzip lalu dihapus).



## Pengerjaan Soal 3
<a href="https://ibb.co/Hn8gZdy"><img src="https://i.ibb.co/9s5YPt0/step1.png" alt="step1" border="0"></a>

Pertama, buatlah direktori dan program file C dengan isi sesuai source code di atas. Kemudian jalankan program dalam **Mode A** dengan command `./lukisan -a`. Setelah program dijalankan pada **Mode A**, maka isi dari folder dan direktori akan menjadi seperti berikut.

<a href="https://ibb.co/fSQtVYf"><img src="https://i.ibb.co/RS7PGpf/isi-folder.png" alt="isi-folder" border="0"></a>
<a href="https://ibb.co/y0FCmbV"><img src="https://i.ibb.co/Y2BS4qb/isi-dir.png" alt="isi-dir" border="0"></a>

Untuk menghentikan proses daemon **Mode A**, gunakan command `CTRL+C`

<a href="https://imgbb.com/"><img src="https://i.ibb.co/0qtz14r/kill.png" alt="kill" border="0"></a>

Lakukan hal yang sama pada proses **Mode B**. Namun, gunakan command yang sedikit berbeda yaitu dengan argumen `-b`.

<a href="https://imgbb.com/"><img src="https://i.ibb.co/HK617W2/run-b.png" alt="run-b" border="0"></a>

Setelah program dijalankan pada **Mode A**, maka isi dari folder dan direktori akan menjadi seperti berikut.

<a href="https://ibb.co/X5WYzw9"><img src="https://i.ibb.co/Ks5K91Y/isi-folder-b.png" alt="isi-folder-b" border="0"></a>
<a href="https://ibb.co/MVgBkVN"><img src="https://i.ibb.co/mqRN5qz/isi-dir-b.png" alt="isi-dir-b" border="0"></a>

Untuk menghentikan proses daemon **Mode B**, gunakan command `CTRL+C`

<a href="https://imgbb.com/"><img src="https://i.ibb.co/S0hBB8X/kill-b.png" alt="kill-b" border="0"></a>


## Kendala Soal 3
Pada soal ini, kami mengalami kendala :
1. Folder terbuat secara otomatis > 30 detik 
2. Proses download foto pada tiap folder tidak bisa overlapping
3. Mode A dan B tidak ada perbedaan serta harus manual dalam menterminasi proses yang sedang berjalan
4. Setelah terzip, folder tidak terhapus

# SOAL 4
Choco adalah seorang ahli pertahanan siber yang tidak suka memakai ChatGPT dalam menyelesaikan masalah. Dia selalu siap melindungi data dan informasi dari ancaman dunia maya. Namun, kali ini, dia membutuhkan bantuan Anda untuk meningkatkan kinerja antivirus yang telah dia buat sebelumnya.

# PROBLEM SOAL 4 A
Bantu Choco dalam mengoptimalkan program antivirus bernama antivirus.c. Program ini seharusnya dapat memeriksa file di folder sisop_infected, dan jika file tersebut diidentifikasi sebagai virus berdasarkan ekstensinya, program harus memindahkannya ke folder quarantine. list dari format ekstensi/tipe file nya bisa didownload di Link Ini , proses mendownload tidak boleh menggunakan system()

# PROBLEM SOAL 4 B
Ada kejutan di dalam file extensions.csv. Hanya 8 baris pertama yang tidak dienkripsi. Baris-baris setelahnya perlu Anda dekripsi menggunakan algoritma rot13 untuk mengetahui ekstensi virus lainnya.Setiap kali program mendeteksi file virus, catatlah informasi tersebut di virus.log. Format log harus sesuai dengan:
[nama_user][Dd-Mm-Yy:Hh-Mm-Ss] - {nama file yang terinfeksi} - {tindakan yang diambil}

Contoh:  [sisopUser][29-09-23:08-59-01] - test.locked - Moved to quarantine

*nama_user: adalah username dari user yang menambahkan file ter-infected

# PROBLEM SOAL 4 C

Dunia siber tidak pernah tidur, dan demikian juga virus. Choco memerlukan antivirus yang terus berjalan di latar belakang tanpa harus dia intervensi. Dengan menjalankan program ini sebagai Latar belakang, program akan secara otomatis memeriksa folder sisop_infected setiap detik.

# PROBLEM SOAL 4 D

Choco juga membutuhkan level-level keamanan antivrus jadi dia membuat 3 level yaitu low, medium ,hard. Argumen tersebut di pakai saat menjalankan antivirus.

    Low: Hanya me-log file yg terdeteksi
    Medium: log dan memindahkan file yang terdeteksi
    Hard: log dan menghapus file yang terdeteksi

ex: ./antivirus -p low

Kadang-kadang, Choco mungkin perlu mengganti level keamanan dari antivirus tanpa harus menghentikannya. Integrasikan kemampuan untuk mengganti level keamanan antivirus dengan mengirim sinyal ke daemon. Misalnya, menggunakan SIGUSR1 untuk mode "low", SIGUSR2 untuk "medium", dan SIGRTMIN untuk mode "hard".

Contoh:

kill -SIGUSR1 <pid_program>

# PROBLEM SOAL 4 E

Meskipun penting untuk menjalankan antivirus, ada saat-saat Choco mungkin perlu menonaktifkannya sementara. Bantu dia dengan menyediakan fitur untuk mematikan antivirus dengan cara yang aman dan efisien.

# SOLUSI SOAL 4 A


```
curl -L -o extensions.csv --insecure 'https://drive.google.com/uc?export=download&id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5'

```
untuk mendownload file csv kita bisa menggunakan program diatas, lalu file akan terdownload dan masuk kedalam extensions.csv

lalu buat folder quarantine dan sisop_infected
```
mkdir quarantine

```
```
mkdir sisop_infected

```
maka seluruh tampilannya akan seperti ini
<a href="https://ibb.co/bLMrr3j"><img src="https://i.ibb.co/82LjjBn/Screenshot-2023-10-13-175224.png" alt="Screenshot-2023-10-13-175224" border="0"></a>

# SOLUSI SOAL 4 B

kita buat terlebih dahulu program c nya dengan cara
```
nano antivirus.c
```

isi dari antivirus.c adalah semua program yang sudah di tentukan pada soal
lalu kita masukkan program untuk mendekrip file yg masi belum terdekrip

```
#include <stdio.h>
#include <string.h>
#include <time.h>

void getTimestamp(char *timestamp) {
    time_t rawtime;
    struct tm *timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);


    strftime(timestamp, 20, "%d-%m-%y:%H-%M-%S", timeinfo);
}

void decryptExtensions(char* filename) {
    FILE *inputFile = fopen(filename, "r");
    FILE *outputFile = fopen("virus.log", "w");  // Membuka file virus.log untuk ditulis

    if (!inputFile || !outputFile) {
        printf("Gagal membuka file.\n");
        return;
    }

    char line[100];
    int lineCount = 0;

    char timestamp[20];
    getTimestamp(timestamp);

    while (fgets(line, sizeof(line), inputFile)) {
        if (lineCount < 8) {
        
            printf("%s", line);

            fprintf(outputFile, "[%s] - %s - %s - Moved to quarantine\n", "sisopUser", timestamp, line);
        } else {
          
            for (int i = 0; i < strlen(line); i++) {
                if ((line[i] >= 'a' && line[i] <= 'z') || (line[i] >= 'A' && line[i] <= 'Z')) {
                    line[i] = (line[i] >= 'a' && line[i] <= 'z') ? (((line[i] - 'a' + 13) % 26) + 'a') : (((line[i] - 'A' + 13) % 26) + 'A');
                }
            }
            printf("%s", line);

            fprintf(outputFile, "[%s] - %s - %s - Moved to quarantine\n", "sisopUser", timestamp, line);
        }

        lineCount++;
    }

    fclose(inputFile);
    fclose(outputFile); 
}

int main() {
    decryptExtensions("extensions.csv");
    return 0;
}

```
program ini akan menjalankan dekrip pada file extensions.csv, lalu akan memindahkan kedalam folder virus.log dengan format yg sudah ditentukan

<a href="https://ibb.co/XY2QgKN"><img src="https://i.ibb.co/MB2v4tW/Screenshot-2023-10-13-175854.png" alt="Screenshot-2023-10-13-175854" border="0"></a>

lalu kita bisa mencompile dengan cara
```
gcc -o antivirus antivirus.c -pthread
```

setelah tercompile program bisa dijalankan dengan cara
```
./antivirus
```

# SOLUSI SOAL 4 C

untuk menjawab soal 4c kita bisa tambahkan codingan ke antivirus.c 

```
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>

void getTimestamp(char *timestamp) {
    time_t rawtime;
    struct tm *timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(timestamp, 20, "%d-%m-%y:%H-%M-%S", timeinfo);
}

void decryptExtensions(char *filename, FILE *logFile, int protectionLevel) {
    FILE *inputFile = fopen(filename, "r");

    if (!inputFile) {
        printf("Gagal membuka file.\n");
        return;
    }

    char line[100];
    int lineCount = 0;

    char timestamp[20];
    getTimestamp(timestamp);

    while (fgets(line, sizeof(line), inputFile)) {
        if (lineCount < 8) {
            printf("%s", line);
            fprintf(logFile, "[%s] - %s - %s - Detected\n", "sisopUser", timestamp, line);

            if (protectionLevel == 2) {
                fprintf(logFile, "[%s] - %s - %s - Moved to quarantine\n", "sisopUser", timestamp, line);
                
            } else if (protectionLevel == 3) {
                fprintf(logFile, "[%s] - %s - %s - Deleted\n", "sisopUser", timestamp, line);
            }
        } else {
            for (int i = 0; i < strlen(line); i++) {
                if ((line[i] >= 'a' && line[i] <= 'z') || (line[i] >= 'A' && line[i] <= 'Z')) {
                    line[i] = (line[i] >= 'a' && line[i] <= 'z') ? (((line[i] - 'a' + 13) % 26) + 'a') : (((line[i] - 'A' + 13) % 26) + 'A');
                }
            }
            printf("%s", line);

            fprintf(logFile, "[%s] - %s - %s - Detected\n", "sisopUser", timestamp, line);

            if (protectionLevel == 2) {
                fprintf(logFile, "[%s] - %s - %s - Moved to quarantine\n", "sisopUser", timestamp, line);
               
            } else if (protectionLevel == 3) {
                fprintf(logFile, "[%s] - %s - %s - Deleted\n", "sisopUser", timestamp, line);
               
            }
        }

        lineCount++;
    }

    fclose(inputFile);
}

void *backgroundScanner(void *arg) {
    int protectionLevel = *((int *)arg);

    FILE *logFile = fopen("virus.log", "a");

    if (!logFile) {
        printf("Gagal membuka virus.log.\n");
        exit(EXIT_FAILURE);
    }

    while (1) {
        decryptExtensions("sisop_infected/extensions.csv", logFile, protectionLevel);
        sleep(1); 

    }

    fclose(logFile);
    return NULL;
}

int main(int argc, char *argv[]) {
    if (argc != 3 || (strcmp(argv[1], "-p") != 0)) {
        printf("Penggunaan: %s -p <low|medium|hard>\n", argv[0]);
        return 1;
    }

    int protectionLevel;
    if (strcmp(argv[2], "low") == 0) {
        protectionLevel = 1;
    } else if (strcmp(argv[2], "medium") == 0) {
        protectionLevel = 2;
    } else if (strcmp(argv[2], "hard") == 0) {
        protectionLevel = 3;
    } else {
        printf("Penggunaan: %s -p <low|medium|hard>\n", argv[0]);
        return 1;
    }

    pthread_t thread;
    pthread_create(&thread, NULL, backgroundScanner, (void *)&protectionLevel);

    printf("Tekan Ctrl+C untuk menghentikan program.\n");
    pthread_join(thread, NULL);

    return 0;
}


```
ini adalah program agar secara otomatis memeriksa didalam file sisop_infectednya

# SOLUSI SOAL 4 D

untuk bisa membedakan tingakatan level menjadi low, medium, dan high kita bisa menggunakna program ini

```
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>

void getTimestamp(char *timestamp) {
    time_t rawtime;
    struct tm *timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(timestamp, 20, "%d-%m-%y:%H-%M-%S", timeinfo);
}

void decryptExtensions(char *filename, FILE *logFile, int protectionLevel) {
    FILE *inputFile = fopen(filename, "r");

    if (!inputFile) {
        printf("Gagal membuka file.\n");
        return;
    }

    char line[100];
    int lineCount = 0;

    char timestamp[20];
    getTimestamp(timestamp);

    while (fgets(line, sizeof(line), inputFile)) {
        if (lineCount < 8) {
            printf("%s", line);
            fprintf(logFile, "[%s] - %s - %s - Detected\n", "sisopUser", timestamp, line);

            if (protectionLevel == 2) {
                fprintf(logFile, "[%s] - %s - %s - Moved to quarantine\n", "sisopUser", timestamp, line);

                char quarantinePath[256];
                sprintf(quarantinePath, "/home/dimas/sisopmodul2/quarantine/%s", filename);
                rename(filename, quarantinePath);
            } else if (protectionLevel == 3) {
                fprintf(logFile, "[%s] - %s - %s - Deleted\n", "sisopUser", timestamp, line);
            }
        } else {
            for (int i = 0; i < strlen(line); i++) {
                if ((line[i] >= 'a' && line[i] <= 'z') || (line[i] >= 'A' && line[i] <= 'Z')) {
                    line[i] = (line[i] >= 'a' && line[i] <= 'z') ? (((line[i] - 'a' + 13) % 26) + 'a') : (((line[i] - 'A' + 13) % 26) + 'A');
                }
            }
            printf("%s", line);

            fprintf(logFile, "[%s] - %s - %s - Detected\n", "sisopUser", timestamp, line);

            if (protectionLevel == 2) {
                fprintf(logFile, "[%s] - %s - %s - Moved to quarantine\n", "sisopUser", timestamp, line);

                char quarantinePath[256];
                sprintf(quarantinePath, "/home/dimas/sisopmodul2/quarantine/%s", filename);
                rename(filename, quarantinePath);
            } else if (protectionLevel == 3) {
                fprintf(logFile, "[%s] - %s - %s - Deleted\n", "sisopUser", timestamp, line);
            }
        }

        lineCount++;
    }

    fclose(inputFile);
}

void *backgroundScanner(void *arg) {
    int protectionLevel = *((int *)arg);

    FILE *logFile = fopen("virus.log", "a");

    if (!logFile) {
        printf("Gagal membuka virus.log.\n");
        exit(EXIT_FAILURE);
    }

    while (1) {
        decryptExtensions("sisop_infected/extensions.csv", logFile, protectionLevel);
        sleep(1);
    }

    fclose(logFile);
    return NULL;
}

int main(int argc, char *argv[]) {
    if (argc != 3 || (strcmp(argv[1], "-p") != 0)) {
        printf("Penggunaan: %s -p <low|medium|hard>\n", argv[0]);
        return 1;
    }

    int protectionLevel;
    if (strcmp(argv[2], "low") == 0) {
        protectionLevel = 1;
    } else if (strcmp(argv[2], "medium") == 0) {
        protectionLevel = 2;
    } else if (strcmp(argv[2], "hard") == 0) {
        protectionLevel = 3;
    } else {
        printf("Penggunaan: %s -p <low|medium|hard>\n", argv[0]);
        return 1;
    }

    pthread_t thread;
    pthread_create(&thread, NULL, backgroundScanner, (void *)&protectionLevel);

    printf("ctrl_c\n");
    pthread_join(thread, NULL);

    return 0;
}

```

program ini akan menjalankan perintah sesuai kebutuhan, tetapi masi ada kendala saat pengecekan mode medium dan hard.
untuk menjalankan program kita bisa menggunakan 
```

./antivirus -p <low,medium,hard>
```

kita memilih mau dijalankan level apa, jika memilih low maka program seperti ini *./antivirus -p low*, maka program akan berjalan dengan level low

## Kendala Soal 4
Pada soal ini, kami mengalami kendala :
1. Masih belum berjalan menjadi daemon 
2. tidak bisa menjalankan level medium dan hard karena tidak terjadi apa", ketika dijalanakan mirip seperti menjalankan level low
3. stuck hanya sampai soal D
4. belum bisa otomatis memberhentikan program








